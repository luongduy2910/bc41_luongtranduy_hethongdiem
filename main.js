// Xây dựng chức năng tạo danh sách điểm SV 

function DanhSachDiemSV() {
    var tdListScore = document.querySelectorAll('.td-scores') ; 
    // Lúc này tdListScore sẽ trả về một NodeList , không phải là Array, tuy nhiên ta có thể sử dụng vòng lặp for để lấy từng giá trị bằng cú pháp .innerText 
    var listScore = [] ; 
    for (var i = 0 ; i < tdListScore.length ; i++) {
        var current = tdListScore[i].innerText ; 
        listScore.push(current) ; 
    }
    return listScore ; 
}

// Xây dựng chức năng tìm ra sinh viên giỏi nhất 

document.querySelector('#btnSVCaoDiemNhat').onclick = function(){timSVCaoDiemNhat()}; 
function timSVCaoDiemNhat() {
    // Lấy danh sách điểm của sinh viên 
    var listScore = DanhSachDiemSV() ; 
    // Trong listScore tìm ra điểm số cao nhất 
    var maxScore = listScore[0] ; 
    for (var i = 1 ; i < listScore.length ; i++) {
        current = listScore[i] ; 
        if (current > maxScore) {
            maxScore = current ; 
        }
    }
    // Tìm vị trí index của số điểm cao nhất để suy ngược lại tên người đang có số điểm cao nhất 
    var IndexofMaxscore = listScore.indexOf(maxScore) ; 
    var tblBody = document.querySelector('#tblBody') ; 
    var trList = tblBody.querySelectorAll("tr") ; 
    var trMax = trList[IndexofMaxscore] ; 
    var tdMax = trMax.querySelectorAll('td') ; 
    var NguoiCoDiemCaoNhat = tdMax[2].innerText ; 
    document.querySelector('#svGioiNhat').innerHTML = `Bạn ${NguoiCoDiemCaoNhat}` ; 

}

// Xây dựng chức năng tìm ra sinh viên kém nhất

document.querySelector('#btnSVThapDiemNhat').onclick = function(){timSVThapDiemNhat()}; 
function timSVThapDiemNhat() {
    // Lấy danh sách điểm của sinh viên 
    var listScore = DanhSachDiemSV() ; 
    // Trong listScore tìm ra điểm số cao nhất 
    var minScore = listScore[0] ; 
    for (var i = 1 ; i < listScore.length ; i++) {
        current = listScore[i] ; 
        if (current < minScore) {
            minScore = current ; 
        }
    }
    // Tìm vị trí index của số điểm cao nhất để suy ngược lại tên người đang có số điểm cao nhất 
    var IndexofMinscore = listScore.indexOf(minScore) ; 
    var tblBody = document.querySelector('#tblBody') ; 
    var trList = tblBody.querySelectorAll("tr") ; 
    var trMin = trList[IndexofMinscore] ; 
    var tdMin = trMin.querySelectorAll('td') ; 
    var NguoiCoDiemThapNhat = tdMin[2].innerText ; 
    document.querySelector('#svYeuNhat').innerHTML = `Bạn ${NguoiCoDiemThapNhat}` ; 

}

// Đếm có bao nhiêu sinh viên giỏi trong danh sách 

document.querySelector("#btnSoSVGioi").addEventListener('click' , function(){
    var listScore = DanhSachDiemSV() ; 
    var listSVgioi = []
    for (var i = 0 ; i < listScore.length ; i++) {
        var current = listScore[i] ; 
        if (current >= 8) {
            listSVgioi.push(current) ; 
        }
    }
    var soSVgioi = listSVgioi.length ;
    document.querySelector("#soSVGioi").innerHTML = `Có ${soSVgioi} SV loại giỏi` ; 

})

// Xây dựng chức năng tạo danh sách sinh viên có điểm TB lớn hơn 5 

document.querySelector('#btnSVDiemHon5').addEventListener('click' , function(){
    var listScore = DanhSachDiemSV() ; 
    var listTBtren5 = listScore.filter(function(n){
        return n > 5 ; 
    }) ; 
    var EachIndextren5 = [] ; 
    for(var i = 0 ; i < listTBtren5.length ; i++) {
        var current = listTBtren5[i] ; 
        var currentIndex = listScore.indexOf(current) ; 
        EachIndextren5.push(currentIndex) ; 
    }
    var tblBody = document.querySelector('#tblBody') ; 
    var trList = tblBody.querySelectorAll("tr") ; 
    var trTBtren5 = [] ; 
    for (var i = 0 ; i < EachIndextren5.length ; i++) {
        var current = EachIndextren5[i] ; 
        var currentIndex = trList[current] ; 
        trTBtren5.push(currentIndex) ; 
    }
    var DanhSach = []
    for (var i = 0 ; i < trTBtren5.length ; i++) {
        var current = trTBtren5[i] ; 
        var tdTBtren5 = current.querySelectorAll('td') ; 
        var SvTBtren5 = tdTBtren5[2].innerText ; 
        DanhSach.push(SvTBtren5) ; 
    }
    document.querySelector('#dsDiemHon5').innerHTML = `${DanhSach}` ; 
    
    
})

// Xây dựng chức năng sắp xếp điểm số tăng dần 

document.querySelector('#btnSapXepTang').addEventListener('click', function(){
    var listScore = DanhSachDiemSV() ; 
    var DiemSVtangdan = listScore.sort((a ,b) => a -b) ; 
    document.querySelector("#dtbTang").innerHTML = `${DiemSVtangdan}` ; 
})


